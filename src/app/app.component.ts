import {Component , AfterViewInit} from '@angular/core';
import {
  DomSanitizer

} from '@angular/platform-browser';


declare var videojs: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  // public videoUrl = 'http://vjs.zencdn.net/v/oceans.mp4';
  public videoUrl;
  // public url = 'rtmp://184.72.239.149/vod/mp4:bigbuckbunny_450.mp4';
  public url = 'rtmp://40.69.56.29:1935/livedrone/dji_screencast_11';
  // public videoUrl = 'rtmp://40.69.56.29:1935/livedrone/dji_screencast_11';
  // public videoUrl = 'rtmp://184.72.239.149/vod/mp4:bigbuckbunny_450.mp4';
  private videoJSplayer: any;


  constructor(private _sanitizer: DomSanitizer) {
    this.videoUrl = this._sanitizer.bypassSecurityTrustUrl(this.url);
  }

  ngAfterViewInit() {
    this.videoJSplayer = videojs(document.getElementById('video_player_id'), {}, function () {
      this.play();
    });
  }

  ngOnDestroy() {
    this.videoJSplayer.dispose();
  }
}
